#include <stdlib.h>

void my_reset_ptr(char **ptr)
{
  free(*ptr);
  *ptr = NULL;
}
