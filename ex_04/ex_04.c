#include <stdlib.h>

char	*my_strdup(char *str)
{
  char	*out;
  int	i;

  i = -1;
  while (str[++i]);

  if ((out = malloc(sizeof(*out) * (i + 1))) == NULL)
    return(NULL);
  
  i = -1;
  while (str[++i])
    out[i] = str[i];

  out[i] = 0;
  return(out);
}
